﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FileSorter
{
    public partial class MainForm : Form
    {
        private readonly string _openFileTextBoxDefault;


        public MainForm()
        {
            InitializeComponent();
            _openFileTextBoxDefault = sourceFileTextBox.Text;
            genKeyButton.Enabled = false;
        }

        private void OpenFileBrowseButton_Click(object sender, EventArgs e)
        {
            var res = openFileDialog.ShowDialog();
            switch (res)
            {
                case (DialogResult.OK):
                    sourceFileTextBox.Text = openFileDialog.FileName;
                    
                    break;
                case (DialogResult.Abort):
                case (DialogResult.Cancel):
                case (DialogResult.None):
                    break;
                default:
                    MessageBox.Show("Invalid Dialog Result", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void SaveFileBrowseButton_Click(object sender, EventArgs e)
        {
            var res = saveFileDialog.ShowDialog();
            switch (res)
            {
                case (DialogResult.OK):
                case (DialogResult.Abort):
                case (DialogResult.Cancel):
                case (DialogResult.None):
                    if (sourceFileTextBox.Text.Trim().Length != 0)
                    {
                        try
                        {
                            var srcPath = sourceFileTextBox.Text;
                            var destPath = saveFileDialog.FileName;
                            SortFileAsync(srcPath, destPath);

                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    break;
                default:
                    MessageBox.Show("Invalid Dialog Result", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void OpenFileTextBox_Enter(object sender, EventArgs e)
        {
            if (sourceFileTextBox.Text.Equals(_openFileTextBoxDefault))
                sourceFileTextBox.Text = "";
        }

        private void OpenFileTextBox_Leave(object sender, EventArgs e)
        {
            if (sourceFileTextBox.Text.Trim().Length == 0)
                sourceFileTextBox.Text = _openFileTextBoxDefault;
        }

        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO--вывод инструкции
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO--вывод информации о приложении
        }

        #region Logic

        private async void SortFileAsync( string fileName,  string destFileName)
        {
            await Task.Run((() => SortFile(fileName, destFileName)));
        }

        private void SortFile(in string fileName, in string destFileName)
        {
            char[] fileData = File.ReadAllText(fileName).ToCharArray();
            BatcherSort.NonRecursiveBatcherSort(fileData, (char c1, char c2) => c2.CompareTo(c1), 0, fileData.Length - 1);
            File.WriteAllText(destFileName, new string(fileData), Encoding.Default);
            MessageBox.Show("Sorted data has been saved to " + destFileName, "OK", MessageBoxButtons.OK);
        }

        #endregion



        private void DragAndPanel_DragDrop(object sender, DragEventArgs e)
        {
            if(e?.Data?.GetData(DataFormats.FileDrop) != null)
            {
                sourceFileTextBox.Text = (e.Data.GetData(DataFormats.FileDrop) as string[])[0];
            }
            dragAndDropPanel.Visible = false;
        }

        private void DragAndDropPanel_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void DragAndDropPanel_DragLeave(object sender, EventArgs e)
        {
            dragAndDropPanel.Visible = false;
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            dragAndDropPanel.Visible = true;
        }

      

        
    }
}