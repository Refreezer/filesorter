﻿using System;
using System.Collections;
using System.Collections.Generic;


class BatcherSort
{
    private static void Swap<T>(T[] collection, int i, int j)
    {
        T tmp = collection[i];
        collection[i] = collection[j];
        collection[j] = tmp;
    }

    private static void InsertionSort<T>(T[] a, int hi, int lo) where T : IComparable
    {
        for (int i = lo; i < hi; ++i)
        {
            int j = i;
            while (j > lo && a[j].CompareTo(a[j - 1]) < 0)
            {
                Swap(a, j - 1, j);
                j--;
            }
        }
    }


    private static void Compexch<T>(T[] array, int i, int j,
        Comparison<T> comparison) where T : IComparable
    {
        if (comparison(array[i], array[j]) < 0)
            Swap(array, i, j);
    }

    private static void Compexch<T>(T[] array, int i, int j) where T : IComparable
    {
        if (array[i].CompareTo(array[j]) < 0)
            Swap(array, i, j);
    }

    private static void Shuffle<T>(T[] sourceArray, int lo, int hi) where T : IComparable
    {
        int mid = (int) (lo + hi) / 2;

        T[] tempArray = new T[sourceArray.Length];

        Array.Copy(sourceArray, tempArray, sourceArray.Length);

        for (int i = lo, j = 0; i <= hi; i += 2, j++)
        {
            tempArray[i] = sourceArray[lo + j];
            tempArray[i + 1] = sourceArray[mid + j + 1];
        }

        for (int i = 0; i < tempArray.Length; i++)
            sourceArray[i] = tempArray[i];
    }

    private static void Unshuffle<T>(T[] sourceArray, int lo, int hi) where T : IComparable
    {
        int mid = (int) (lo + hi) / 2;

        T[] tempArray = new T[sourceArray.Length];

        Array.Copy(sourceArray, tempArray, sourceArray.Length);

        for (int i = lo, j = 0; i <= hi; i += 2, j++)
        {
            tempArray[lo + j] = sourceArray[i];
            tempArray[mid + j + 1] = sourceArray[i + 1];
        }

        for (int i = 0; i < tempArray.Length; i++)
            sourceArray[i] = tempArray[i];
    }

    private static void Merge<T>(T[] abomination, T[] parts, int lo, int mid, int hi) where T : IComparable
    {
        for (int k = lo; k <= hi; ++k)
            parts[k] = abomination[k];

        int i = lo, j = mid + 1;
        for (int k = lo; k <= hi; ++k)
        {
            if (i > mid)
            {
                abomination[k] = parts[j++];
            }
            else if (j > hi)
            {
                abomination[k] = parts[i++];
            }
            else if (parts[j].CompareTo(parts[i]) < 0)
            {
                abomination[k] = parts[j++];
            }
            else
            {
                abomination[k] = parts[i++];
            }
        }
    }

    private static void OddEvenMergeSort<T>(T[] sourceArray, int lo, int hi) where T : IComparable
    {
        if (hi == lo + 1)
            Compexch(sourceArray, lo, hi); //мы дошли до подмассива размера 2 - теперь просто сравним элементы
        if (hi < lo + 2) return; //дошли до подмассива размера 1 - выходим, такой подмассив априори отсортирован


        Unshuffle(sourceArray, lo, hi); //делим подмассив на две части
        int mid = (int) (lo + hi) / 2;

        OddEvenMergeSort(sourceArray, lo, mid);
        OddEvenMergeSort(sourceArray, mid + 1, hi); //вызываемся рекурсивно для половинок
        Shuffle(sourceArray, lo, hi); //сливаем части

        //{3,4,2,1} {3,2,4,1} {2,3,1,4}

        for (int i = lo + 1; i < hi; i += 2)
            Compexch(sourceArray, i, i + 1);

        int halfSize = (hi - lo + 1) / 2 - 1; //*

        for (int i = lo + 1; i + halfSize < hi; i++) //*
            Compexch(sourceArray, i, i + halfSize); //*
    }

    public void OddEvenMergeSort<T>(T[] sourceArray) where T : IComparable
    {
        BitArray sizes = new BitArray(BitConverter.GetBytes(sourceArray.Length));

        int i = 0; //sourceArray.Length : int

        int sourceIndex = 0;
        foreach (bool size in sizes)
        {
            if (size == true)
            {
                int sizeOfNewArray = (int) Math.Pow(2, i);
                OddEvenMergeSort(sourceArray, sourceIndex, sourceIndex + sizeOfNewArray);
                sourceIndex += sizeOfNewArray;
                i++;
            }
        }
    }

    public static void NonRecursiveBatcherSort<T>(T[] sourceArray, Comparison<T> comparison,
        int lo = 0, int hi = -1) where T : IComparable
    {
        if (hi == -1)
            hi = sourceArray.Length;

        int n = hi - lo + 1;
        for (int p = 1; p < n; p += p)
        {
            for (int k = p; k > 0; k /= 2)
            {
                for (int j = k % p; j + k < n; j += (k + k))
                {
                    for (int i = 0; i < n - j - k; ++i)
                    {
                        if ((j + i) / (p + p) == (j + i + k) / (p + p))
                            Compexch(sourceArray, lo + j + i, lo + j + i + k, comparison);
                    }
                }
            }
        }
    }
}