﻿namespace FileSorter
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sourceFileTextBox = new System.Windows.Forms.TextBox();
            this.OpenFileBrowseButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.genKeyButton = new System.Windows.Forms.Button();
            this.dragAndDropPanel = new System.Windows.Forms.Panel();
            this.dndLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.dragAndDropPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(318, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.HelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // sourceFileTextBox
            // 
            this.sourceFileTextBox.Location = new System.Drawing.Point(12, 37);
            this.sourceFileTextBox.Name = "sourceFileTextBox";
            this.sourceFileTextBox.Size = new System.Drawing.Size(214, 20);
            this.sourceFileTextBox.TabIndex = 1;
            this.sourceFileTextBox.Text = "Source file path...";
            this.sourceFileTextBox.Enter += new System.EventHandler(this.OpenFileTextBox_Enter);
            this.sourceFileTextBox.Leave += new System.EventHandler(this.OpenFileTextBox_Leave);
            // 
            // OpenFileBrowseButton
            // 
            this.OpenFileBrowseButton.Location = new System.Drawing.Point(232, 37);
            this.OpenFileBrowseButton.Name = "OpenFileBrowseButton";
            this.OpenFileBrowseButton.Size = new System.Drawing.Size(75, 20);
            this.OpenFileBrowseButton.TabIndex = 3;
            this.OpenFileBrowseButton.Text = "Browse...";
            this.OpenFileBrowseButton.UseVisualStyleBackColor = true;
            this.OpenFileBrowseButton.Click += new System.EventHandler(this.OpenFileBrowseButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(232, 76);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.SaveFileBrowseButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // genKeyButton
            // 
            this.genKeyButton.Location = new System.Drawing.Point(151, 76);
            this.genKeyButton.Name = "genKeyButton";
            this.genKeyButton.Size = new System.Drawing.Size(75, 23);
            this.genKeyButton.TabIndex = 5;
            this.genKeyButton.Text = "Gen last key";
            this.genKeyButton.UseVisualStyleBackColor = true;
            // 
            // dragAndDropPanel
            // 
            this.dragAndDropPanel.AllowDrop = true;
            this.dragAndDropPanel.Controls.Add(this.dndLabel);
            this.dragAndDropPanel.Location = new System.Drawing.Point(0, 0);
            this.dragAndDropPanel.Name = "dragAndDropPanel";
            this.dragAndDropPanel.Size = new System.Drawing.Size(318, 113);
            this.dragAndDropPanel.TabIndex = 6;
            this.dragAndDropPanel.Visible = false;
            this.dragAndDropPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragAndPanel_DragDrop);
            this.dragAndDropPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragAndDropPanel_DragEnter);
            this.dragAndDropPanel.DragLeave += new System.EventHandler(this.DragAndDropPanel_DragLeave);
            // 
            // dndLabel
            // 
            this.dndLabel.AutoSize = true;
            this.dndLabel.Font = new System.Drawing.Font("Lucida Handwriting", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dndLabel.Location = new System.Drawing.Point(106, 38);
            this.dndLabel.Name = "dndLabel";
            this.dndLabel.Size = new System.Drawing.Size(111, 19);
            this.dndLabel.TabIndex = 0;
            this.dndLabel.Text = "Drag and drop";
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 111);
            this.Controls.Add(this.dragAndDropPanel);
            this.Controls.Add(this.genKeyButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.OpenFileBrowseButton);
            this.Controls.Add(this.sourceFileTextBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "File Sorter";
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.dragAndDropPanel.ResumeLayout(false);
            this.dragAndDropPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox sourceFileTextBox;
        private System.Windows.Forms.Button OpenFileBrowseButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button genKeyButton;
        private System.Windows.Forms.Panel dragAndDropPanel;
        private System.Windows.Forms.Label dndLabel;
    }
}

